from Tkinter import *
import math

class App:

 def __init__(self,master):
            
  #Frame.__init__(self, master)

  #self.LastClick= Label(self, text= "Last Point Clicked:")
  #self.LastClick.pack(side= LEFT)

  #self.CurrentPosition= Label(self, text="Cursor Point:")
  #self.CurrentPosition.pack(side= LEFT)

  self.master=master
  self.sc_value1x=0
  self.sc_value1y=0
  self.sc_value2x=0
  self.sc_value2y=0
  self.sc_value1_flag=0
  self.sc_value2_flag=0
  self.value1x=0
  self.value1y=0
  self.value2x=0
  self.value2y=0
  self.value1_flag=0
  self.value2_flag=0
  self.sc_distance=0
  self.get_scale_points=0
  self.input_value_flag=0
    
  master.geometry("400x400")
  
            
  frame3=Frame(master)  # The Labels
  frame3.pack(side=LEFT)

  frame2=Frame(master)  # The entrys
  frame2.pack(side=LEFT)

  frame1=Frame(master)   #The hello and quit buttons
  frame1.pack(side=LEFT)

  self.button1 = Button(frame1, text="QUIT", fg="red", command=master.quit)
  self.button1.pack(side=TOP)
  
  self.button2 = Button(frame1, text="Reset", fg="red", command=self.reset_values)
  self.button2.pack(side=TOP)
  
  self.button3 = Button(frame1, text="Input Scale Distance", fg="red", command=self.input_scale)
  self.button3.pack(side=TOP)
  
  self.button4 = Button(frame1, text="Input Scale Values", fg="red", command=self.input_scale_values)
  self.button4.pack(side=TOP)
  
  self.button5 = Button(frame1, text="Debug", fg="red", command=self.debug)
  self.button5.pack(side=TOP)
  
  self.button6 = Button(frame1, text="Input Values", fg="red", command=self.input_values)
  self.button6.pack(side=TOP)
  
  self.button7 = Button(frame1, text="Compute Distance", fg="red", command=self.compute_distance)
  self.button7.pack(side=TOP)
  master.wait_visibility(master)  
  master.attributes('-alpha',0.5)
  
  
  master.bind('<Button-1>',self.button_click)
	
      
 def popup(self,x,y):
    
  other=Toplevel()
  other.title('Confirmation')
  other.e_label1=Label(other,text="X_coord: "+str(x))
  other.e_label1.pack(side=TOP)
  other.e_label2=Label(other,text="Y_coord: "+str(y))
  other.e_label2.pack(side=TOP)
  
  other.e_button1=Button(other, text="Confirmation", fg="red", command= lambda: self.store_value(other,x,y))
  other.e_button1.pack(side=BOTTOM)
  
  other.button2=Button(other, text="Cancel", fg="red", command=other.destroy)
  other.button2.pack(side=TOP)
  
 def button_click(self,event):
 
  if (self.input_value_flag==1) or (self.get_scale_points==1): 
   self.popup(event.x,event.y)
  return  
  
 def store_value(self,other,x,y):
  
  if self.get_scale_points==1:
   if self.sc_value1_flag==0:
   
    self.sc_value1x=float(x)
    self.sc_value1y=float(y)
    self.sc_value1_flag=1
    print(self.sc_value1_flag)
    
   elif self.sc_value1_flag==1 and self.sc_value2_flag==0:
    self.sc_value2x=float(x)
    self.sc_value2y=float(y)
    self.sc_value2_flag=1
    self.get_scale_points=0
    print(self.sc_value2_flag)
  elif self.input_value_flag==1:
   if self.value1_flag==0:
   
    self.value1x=float(x)
    self.value1y=float(y)
    self.value1_flag=1
    print(self.value1_flag)
    
   elif self.value1_flag==1 and self.value2_flag==0:
    self.value2x=float(x)
    self.value2y=float(y)
    self.value2_flag=1
    self.input_value_flag=0
    print(self.value2_flag)
    
  other.destroy()
  
 def reset_values(self):
  self.sc_value1x=0
  self.sc_value1y=0
  self.sc_value2x=0
  self.sc_value2y=0
  self.sc_value1_flag=0
  self.sc_value2_flag=0
  self.value1x=0
  self.value1y=0
  self.value2x=0
  self.value2y=0
  self.value1_flag=0
  self.value2_flag=0
  self.sc_distance=0
  self.get_scale_points=0
  self.input_value_flag=0
 
 def input_scale(self):
   scale=Toplevel()
   
   scale.label1=Label(scale, text='Distance:')
   scale.label1.pack(side=LEFT)
   
   scale.e1=Entry(scale)
   scale.e1.pack(side=LEFT)
   
   scale.e_button1=Button(scale, text="Confirmation", fg="red", command= lambda: self.store_distance_value(scale))
   scale.e_button1.pack(side=BOTTOM)
  
   scale.button2=Button(scale, text="Cancel", fg="red", command=scale.destroy)
   scale.button2.pack(side=TOP)
 def store_distance_value(self,scale):
  self.sc_distance= float(scale.e1.get())
  scale.destroy()
 def debug(self):
  print "scale values"
  print self.sc_value1x
  print self.sc_value1y
  print self.sc_value2x
  print self.sc_value2y
  print 'points to do values'
  print self.value1x
  print self.value1y
  print self.value2x
  print self.value2y
  print 'flags'
  print self.value1_flag
  print self.value2_flag
  print self.get_scale_points
  print self.input_value_flag
  print 'scale distance'
  print self.sc_distance
 def input_scale_values(self):
  self.get_scale_points=1
  self.sc_value1_flag=0
  self.sc_value2_flag=0
 def input_values(self):
  self.input_value_flag=1
  self.value1_flag=0
  self.value2_flag=0
 def compute_distance(self):
  
  temp1=math.sqrt((self.sc_value1x-self.sc_value2x)*(self.sc_value1x-self.sc_value2x)+(self.sc_value1y-self.sc_value2y)*(self.sc_value1y-self.sc_value2y))  
  temp2=math.sqrt((self.value1x-self.value2x)*(self.value1x-self.value2x)+(self.value1y-self.value2y)*(self.value1y-self.value2y))
  
  distance=(temp2/temp1)*self.sc_distance
  
  output=Toplevel()
 
  output.e_label1=Label(output,text="Distance: "+str(distance))
  output.e_label1.pack(side=TOP)
  
  
root= Tk()

app=App(root)
root.mainloop()


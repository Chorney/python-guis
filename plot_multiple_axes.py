# -*- coding: utf-8 -*-
"""
Created on Thu May 07 13:25:44 2015

@author: Drew
"""
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt
from Tkinter import *

from MyLibrary.load_fieldMS_data import *

from MyLibrary.mathfunctions import *
from MyLibrary.management_functions import *
from MyLibrary.conversions import *
from MyLibrary.microseismic_objects import *

#open's please enter format
#checkbox, insite,pinnacle,rdv


#load microseismic
Well=[]
ms_file='ENAP_CabW_ZG1_MS_Events_Frac_Part_2.csv'
Well=load_ms_insite(ms_file,Well)

#treatment data
myfile=open('ENAP_CabW_Frac_Part_2.csv')
data1=myfile.readlines()
myfile.close()
#--------------------------------------------------------------------------
data1.pop(0)

headers1=data1[0]
headers1=headers1.split(',')


data1.pop(0)

pump_dict=dict([])

for t in range(0,len(headers1)):
    list_temp=[]
    for s in range(0,len(data1)):
        temp=data1[s]
        temp=temp.split(',')
        
        if headers1[t]=='Time':
            blah=temp[t]
            blah=blah.split(':')
            pump_time=float(blah[0])+(float(blah[1])+float(blah[2])/60.0)/60.0
            list_temp.append(pump_time)
        elif headers1[t]=='Date':
            list_temp.append(temp[t])
        else:
            list_temp.append(float(temp[t]))
    
    pump_dict[headers1[t]]=list_temp

#---------------------------------------------------------------------------

header_test=[['WH Pressure (PSI)','red',4000],['WH Proppant Conc. (PPA)','green',20],['BH Proppant Conc. (PPA)\n','yellow',20]]
#input time length,num bins
length_time=18.6-15.5
num_intervals=50

time_interval=[]

for s in range(0,num_intervals):
    time_interval.append(s*(length_time/num_intervals)+15.5)
    
subset_time=[]
well_time=[]
for s in range(0,len(Well)):
    well_time.append(Well[s].time)
    if Well[s].located==1.0:
        subset_time.append(Well[s].time)


    
output=bin_data(well_time,time_interval)
events=bin_data(subset_time,time_interval)    
    
host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)


par=[]
par.append(host.twinx())    
offset_inc=80
offset=0

for s in range(1,len(header_test)):
    
    par.append(host.twinx())    
            
    offset = offset+offset_inc
    new_fixed_axis = par[s].get_grid_helper().new_fixed_axis
    par[s].axis["right"] = new_fixed_axis(loc="right",
                                        axes=par[s],
                                        offset=(offset, 0))
    
    par[s].axis["right"].toggle(all=True)
    
host.set_xlim(15.5, 18.6)
host.set_ylim(0, 60)

host.set_xlabel("Time (hours)",fontsize=20)
host.set_ylabel("Event Count",fontsize=20)

for s in range(0,len(header_test)):
    par[s].set_ylabel(header_test[s][0])


host.bar(output[0],output[1],0.05,label="Triggered",color=[0.588,0.588,0.588])
host.bar(events[0],events[1],0.05,label="Located",color=[0.196,0.392,0.588])

p=[]
for s in range(0,len(header_test)):
    p.append(par[s].plot(pump_dict['Time'],pump_dict[header_test[s][0]],color=header_test[s][1])[0])

for s in range(0,len(header_test)):
    par[s].set_ylim(0, header_test[s][2])


host.legend()

host.axis["left"].label.set_fontsize(16)
host.axis["bottom"].label.set_fontsize(16)

for s in range(0,len(header_test)):
    par[s].axis["right"].label.set_color(p[s].get_color())
    par[s].axis["right"].label.set_fontsize(16)


plt.draw()
plt.show()
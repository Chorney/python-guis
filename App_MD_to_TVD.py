# -*- coding: utf-8 -*-
"""
Created on Tue Jul 07 10:05:27 2015

@author: Drew
"""

import numpy as np
from Tkinter import *
import os
import tkFileDialog
import math

def get_xyz(deviation_survey,MD):

    MD_series=deviation_survey[0]
    x=deviation_survey[1]
    y=deviation_survey[2]
    z=deviation_survey[3]
    
    [left_index,right_index]=get_locations_before_after(MD_series,MD)
    pt1=[x[left_index],y[left_index],z[left_index]]
    pt2=[x[right_index],y[right_index],z[right_index]]
       
    pt=interpolate2D(pt1,pt2,MD-MD_series[left_index])
    
    return pt
    
def get_locations_before_after(series,value):

    temp=np.abs(np.subtract(series,value))
    spot=np.argmin(temp)
    
    #print spot
    
    if (value-series[spot]) > 0:
        left_index=spot
        right_index=spot+1
    else:
        left_index=spot-1
        right_index=spot
        
    return[left_index,right_index]
    
def interpolate2D(pt1,pt2,length_along_vector):

	vec=np.subtract(pt2,pt1)
	vec=get_unit_vector(vec)

	pt=np.add(np.multiply(vec,length_along_vector),pt1)
	
	pt=[pt[0],pt[1],pt[2]]
	
	return pt
 
def get_unit_vector(v):
    
    if len(v)==3:
        v_unit=np.divide(v,np.linalg.norm(v))
        v_unit=[v_unit[0],v_unit[1],v_unit[2]]
    else:
        v_unit=np.divide(v,np.linalg.norm(v))
        v_unit=[v_unit[0],v_unit[1]]
    
    return v_unit


class App:
    
    def __init__(self,master):
            
    #Frame.__init__(self, master)
    
    #self.LastClick= Label(self, text= "Last Point Clicked:")
    #self.LastClick.pack(side= LEFT)
    
    #self.CurrentPosition= Label(self, text="Cursor Point:")
    #self.CurrentPosition.pack(side= LEFT)
    
        current_dir=os.popen('cd').read()   #cd is the MS command prompt command for pwd
        current_dir=current_dir[0:-1]  #Remove the /n character
        current_dir.replace("\\", "\\\\") #NOTE: "\\" is actually '\', this has to deal with \ being an escape character
        
        self.current_dir=current_dir
    
        self.master=master
        self.sc_value1x=0
        self.sc_value1y=0
        self.sc_value2x=0
        self.sc_value2y=0
        self.sc_value1_flag=0
        self.sc_value2_flag=0
        self.value1x=0
        self.value1y=0
        self.value2x=0
        self.value2y=0
        self.value1_flag=0
        self.value2_flag=0
        self.sc_distance=0
        self.get_scale_points=0
        self.input_value_flag=0
          
        master.geometry("400x400")
          
                    
        frame3=Frame(master)  # The Labels
        frame3.pack(side=LEFT)
        
        frame2=Frame(master)  # The entrys
        frame2.pack(side=LEFT)
        
        frame1=Frame(master)   #The hello and quit buttons
        frame1.pack(side=LEFT)
        
        #self.button1 = Button(frame1, text="QUIT", fg="red", command=master.quit)
        #self.button1.pack(side=TOP)
          
        self.button2 = Button(frame1, text="Import Deviation File", fg="red", command=self.import_log)
        self.button2.pack(side=TOP)
        
        self.button3 = Button(frame1, text="Enter MD", fg="red", command=self.import_MD)
        self.button3.pack(side=TOP)
        
        self.button4 = Button(frame1, text="Compute Individual TVD", fg="red", command=self.compute_TVD)
        self.button4.pack(side=TOP)
        
        self.button5 = Button(frame1, text="Enter MD File", fg="red", command=self.import_MDs)
        self.button5.pack(side=TOP)
               
        self.button6 = Button(frame1, text="Compute Multiple TVDs", fg="red",command=self.compute_mult_TVDs)
        self.button6.pack(side=TOP)
               
    def import_log(self):
        filename = tkFileDialog.askopenfilename(filetypes = (("Template files", "*.csv")
                                                             ,("All files", "*.*") ))
        self.log_file_name=filename
        
        temp=filename.split('/')
        main_dir=''
        for s in range(0,len(temp)-1):
            main_dir=main_dir+'/'+temp[s]
            
        self.current_dir=main_dir
        
    def import_MDs(self):
        filename = tkFileDialog.askopenfilename(filetypes = (("Template files", "*.csv")
                                                             ,("All files", "*.*") ),initialdir=self.current_dir)
        self.MD_file_name=filename
                
    def store_md_entry(self,md):
        self.md=md.e1.get()
        md.destroy()
        
    def import_MD(self):
        md=Toplevel()
        md.label1=Label(md, text='MD:')
        md.label1.pack(side=LEFT)
        
        md.e1=Entry(md)
        md.e1.pack(side=LEFT)
        
        md.e_button1=Button(md, text="Confirmation", fg="red", command= lambda: self.store_md_entry(md))
        md.e_button1.pack(side=BOTTOM)
        
        md.button2=Button(md, text="Cancel", fg="red", command=md.destroy)
        md.button2.pack(side=TOP)
        try:
            self.text1.delete(0,END)
        except:
            pass
        
    def compute_TVD(self):

        myfile=open(self.log_file_name)        
        data=myfile.readlines()        
        myfile.close()
        
        data.pop(0)
        num_elem=len(data)
        
        x=[]
        y=[]
        z=[]
        MD=[]
        
        for s in range(0,num_elem):
            temp=data[s]
            temp=temp.split(',')
            x.append(float(temp[5]))
            y.append(float(temp[4]))
            z.append(float(temp[3]))
            MD.append(float(temp[0]))
        
        deviation_survey=[MD,x,y,z]
        
        #return deviation_survey
        
        self.pt=get_xyz(deviation_survey,float(self.md))
    
        blah=Toplevel()
        blah.text1=Text(blah) #output,text="TVD: "+str(self.pt[0])+','+str(self.pt[1])+','+str(self.pt[2]))
        blah.text1.insert(END,"x: "+str(self.pt[0])+'\n'+'y: '+str(self.pt[1])+'\n'+'z: '+str(self.pt[2])+'\n')
        blah.text1.config(state=DISABLED)
        blah.text1.pack(side=TOP)
        blah.text1.delete(1.0, END)
        
    def compute_mult_TVDs(self):
        
        export_file_name = tkFileDialog.asksaveasfilename(filetypes = (("Template files", "*.csv")
                                                             ,("All files", "*.*") ),initialdir=self.current_dir)
        
        current_dir=os.popen('cd').read()   #cd is the MS command prompt command for pwd
        current_dir=current_dir[0:-1]  #Remove the /n character
        current_dir.replace("\\", "\\\\") #NOTE: "\\" is actually '\', this has to deal with \ being an escape character
        
        myfile=open(self.log_file_name)        
        data=myfile.readlines()        
        myfile.close()
        
        data.pop(0)
        num_elem=len(data)
        
        x=[]
        y=[]
        z=[]
        MD=[]
        
        for s in range(0,num_elem):
            temp=data[s]
            temp=temp.split(',')
            x.append(float(temp[5]))
            y.append(float(temp[4]))
            z.append(float(temp[3]))
            MD.append(float(temp[0]))
            
        myfile=open(self.MD_file_name)
        data2=myfile.readlines()
        myfile.close()

        data2.pop(0)
            
        if export_file_name[-3:]!='csv':
            export_file_name=export_file_name+'.csv'
            
        myfile=open(export_file_name,'w')
        myfile.write('x,y,z\n')
        for s in range(0,len(data2)):
            MD_input=float(data2[s])            
            
            deviation_survey=[MD,x,y,z]
            pt=get_xyz(deviation_survey,MD_input)
        
            myfile.write(str(pt[0])+','+str(pt[1])+','+str(pt[2])+'\n')
        
        
        myfile.close()
        

root= Tk()

app=App(root)
root.mainloop()
# -*- coding: utf-8 -*-
"""
Created on Thu May 07 13:25:44 2015

@author: Drew
"""
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt
from Tkinter import *
import tkFileDialog

from MyLibrary.load_fieldMS_data import *

from MyLibrary.mathfunctions import *
from MyLibrary.management_functions import *
from MyLibrary.conversions import *
from MyLibrary.microseismic_objects import *

#open's please enter format
#checkbox, insite,pinnacle,rdv

class App:
    
    def __init__(self,master):
            
    #Frame.__init__(self, master)
    
    #self.LastClick= Label(self, text= "Last Point Clicked:")
    #self.LastClick.pack(side= LEFT)
    
    #self.CurrentPosition= Label(self, text="Cursor Point:")
    #self.CurrentPosition.pack(side= LEFT)
    
        current_dir=os.popen('cd').read()   #cd is the MS command prompt command for pwd
        current_dir=current_dir[0:-1]  #Remove the /n character
        current_dir.replace("\\", "\\\\") #NOTE: "\\" is actually '\', this has to deal with \ being an escape character
        
        self.current_dir=current_dir   
        self.master=master
        self.first_time_frac_attributes=1
          
        master.geometry("400x400")
          
                    
        frame3=Frame(master)  # The Labels
        frame3.pack(side=LEFT)
        
        frame2=Frame(master)  # The entrys
        frame2.pack(side=LEFT)
        
        frame1=Frame(master)   #The hello and quit buttons
        frame1.pack(side=LEFT)
        
        #self.button1 = Button(frame1, text="QUIT", fg="red", command=master.quit)
        #self.button1.pack(side=TOP)
          
        self.button2 = Button(frame1, text="Load Events", fg="red", command=self.load_events)
        self.button2.pack(side=TOP)
        
        self.button3 = Button(frame1, text="Load Frac Data", fg="red", command=self.load_frac_data)
        self.button3.pack(side=TOP)
        
        self.button4 = Button(frame1, text="Set frac attributes", fg="red", command=self.get_frac_attributes)
        self.button4.pack(side=TOP)
        
        self.button5 = Button(frame1, text="Set other attributes",fg="red",command=self.get_other_attributes)
        self.button5.pack(side=TOP)
                    
        self.button5 = Button(frame1, text="Make plot", fg="red",command=self.make_plot)
        self.button5.pack(side=TOP)
        
        
               
    def load_events(self):
        ms_file = tkFileDialog.askopenfilename(filetypes = (("Template files", "*.csv")
                                                             ,("All files", "*.*") ))
        self.ms_file=ms_file
        
        #load microseismic
        self.Well=[]
        self.Well=load_ms_insite(ms_file,self.Well)
        
        temp=ms_file.split('/')
        main_dir=''
        for s in range(0,len(temp)-1):
            main_dir=main_dir+'/'+temp[s]
            
        self.current_dir=main_dir
        
    def load_frac_data(self):
        frac_filename = tkFileDialog.askopenfilename(filetypes = (("Template files", "*.csv")
                                                             ,("All files", "*.*") ),initialdir=self.current_dir)
        self.frac_filename=frac_filename
        
        #treatment data
        myfile=open(frac_filename)
        data1=myfile.readlines()
        myfile.close()
        #--------------------------------------------------------------------------
        data1.pop(0)
        
        self.frac_headers=data1[0]
        self.frac_headers=self.frac_headers.split(',')
                
        data1.pop(0)
        
        self.pump_dict=dict([])
        
        for t in range(0,len(self.frac_headers)):
            list_temp=[]
            for s in range(0,len(data1)):
                temp=data1[s]
                temp=temp.split(',')
                
                if self.frac_headers[t]=='Time':
                    blah=temp[t]
                    blah=blah.split(':')
                    pump_time=float(blah[0])+(float(blah[1])+float(blah[2])/60.0)/60.0
                    list_temp.append(pump_time)
                elif self.frac_headers[t]=='Date':
                    list_temp.append(temp[t])
                else:
                    list_temp.append(float(temp[t]))
            
            self.pump_dict[self.frac_headers[t]]=list_temp
        
                
    def get_frac_attributes(self):
        
        att_e=Toplevel()        
        att_e.geometry("800x400")        
        
        att_e.label1=Label(att_e, text = 'Color in RGB format (ex. format 100,255,0)')
        att_e.label1.pack(side=TOP)
        att_e.label1=Label(att_e, text = '')
        att_e.label1.pack(side=TOP)
        
        self.var=[]
        att_e.frames=[]
        att_e.color_entbox=[]
        att_e.upper_entbox=[]
        self.frac_headers_to_use=[]
        s=0
        for header in self.frac_headers:

            if header!='Time' and header!='Date':
                att_e.frames.append(Frame(att_e))
                att_e.frames[-1].pack(side=TOP)
                self.var.append(IntVar())
                chkbutton=Checkbutton(att_e.frames[-1],text=header,variable=self.var[-1])
                chkbutton.pack(side=LEFT)
                
                chkbox=Label(att_e.frames[-1],text='Color')
                chkbox.pack(side=LEFT)
                
                att_e.color_entbox.append(Entry(att_e.frames[-1],width=10))
                att_e.color_entbox[-1].pack(side=LEFT)
                
                chkbox=Label(att_e.frames[-1],text='Upper Bound')
                chkbox.pack(side=LEFT)
                
                att_e.upper_entbox.append(Entry(att_e.frames[-1],width=10))
                att_e.upper_entbox[-1].pack(side=LEFT)
                
                if self.first_time_frac_attributes==1:
                    att_e.upper_entbox[-1].insert(0,str(np.max(self.pump_dict[header])))
                    att_e.color_entbox[-1].insert(0,str(np.round(np.random.uniform(0,255)))+','+str(np.round(np.random.uniform(0,255)))+','+str(np.round(np.random.uniform(0,255))))
                else:
                    att_e.upper_entbox[-1].insert(0,self.upper_entbox[s])
                    att_e.color_entbox[-1].insert(0,self.color_entbox[s])
                    
                s=s+1
                        
                self.frac_headers_to_use.append(header)
        
        att_e.button1=Button(att_e,text='Done',command= lambda: self.store_entries(att_e))
        att_e.button1.pack(side=BOTTOM)
        
    def store_entries(self,att_e):
        self.color_entbox=[]
        self.upper_entbox=[]
        
        for s in range(0,len(att_e.upper_entbox)):
            self.color_entbox.append(att_e.color_entbox[s].get())
            self.upper_entbox.append(att_e.upper_entbox[s].get())
        
        self.first_time_frac_attributes=0
        att_e.destroy()
        
    def get_other_attributes(self):
        
        other_att=Toplevel()
        other_att.geometry("400x400")
        
        self.other_texts=['Day Start','Time Start','Day End','Time End','Bin Num','Max Event Count','Max Distance']
        other_frames=[]
        self.other_entries=[]        
        
        for s in range(0,len(self.other_texts)):        
            other_frames.append(Frame(other_att))
            other_frames[s].pack(side=TOP)                  
            txt=Label(other_frames[s],text=self.other_texts[s])
            txt.pack(side=LEFT)            
            self.other_entries.append(Entry(other_frames[s],width=10))
            self.other_entries[s].pack(side=LEFT)

        self.other_entries[0].insert(0,self.pump_dict['Date'][0])        
        self.other_entries[1].insert(0,str(np.min(self.pump_dict['Time'])))
        
        self.other_entries[2].insert(0,self.pump_dict['Date'][-1])   
        self.other_entries[3].insert(0,str(np.max(self.pump_dict['Time'])))
        
        choice_frame1=Frame(other_att)
        choice_frame1.pack(side=TOP)
        
        self.choice_val1=1
        self.choice_val2=1

        self.v1=IntVar()
        self.v1.set(1)
        
        b1=Radiobutton(choice_frame1,text='Histogram',padx=20,variable=self.v1,command=self.choose_choice,value=1).pack(anchor=W)
        b2=Radiobutton(choice_frame1,text='Points',padx=20,variable=self.v1,command=self.choose_choice,value=2).pack(anchor=W)
        
        choice_frame2=Frame(other_att)
        choice_frame2.pack(side=TOP)  
        
        self.v2=IntVar()
        self.v2.set(1)
        
        b1=Radiobutton(choice_frame2,text='Vertical Distance',padx=20,variable=self.v2,command=self.choose_choice,value=1).pack(anchor=W)
        b2=Radiobutton(choice_frame2,text='Lateral Distance',padx=20,variable=self.v2,command=self.choose_choice,value=2).pack(anchor=W)
                        
        other_att.button1=Button(other_att,text='Done',command = lambda: self.store_other_entries(other_att))
        other_att.button1.pack(side=BOTTOM)
        
    def choose_choice(self):       
        self.choice_val1=self.v1.get()
        self.choice_val2=self.v2.get()
        
    def store_other_entries(self,other_att):
        
        self.date_start=self.other_entries[0].get()
        self.time_start=self.other_entries[1].get()
        self.date_end=self.other_entries[2].get()
        self.time_end=self.other_entries[3].get()
        self.bin_num=self.other_entries[4].get()
        self.max_event_count=self.other_entries[5].get()
        self.max_distance=self.other_entries[6].get()
        
        other_att.destroy()
        
    def make_plot(self):
        
        header_test=[]
        for s in range(0,len(self.var)):
            
            if self.var[s].get()==1:
                color_rgb=self.color_entbox[s]
                try:
                    color_rgb=np.divide(list_float(color_rgb.split(',')),256.0)
                except:
                    pass
            
                header_test.append([self.frac_headers_to_use[s],color_rgb,float(self.upper_entbox[s])])
                
        #input time length,num bins
        num_days=days_between_dates(self.date_start,self.date_end)
        length_time=float(self.time_end)-float(self.time_start)+24*num_days
        
        print length_time
        
                
        time_interval=[]
        
        for s in range(0,int(self.bin_num)):
            time_interval.append(s*(length_time/float(self.bin_num))+0)
            

        well_time=[]
        subset_time=[]
        evt_mags=[]
        evt_vert_dist=[]
        evt_lat_dist=[]
        evt_color=[]
        for s in range(0,len(self.Well)):
            
            num_days=days_between_dates(self.date_start,convert_date1_date2(self.Well[s].date))
            length_time_temp=float(self.Well[s].time)-float(self.time_start)+24*num_days          
            
            well_time.append(length_time_temp)
            
            if self.Well[s].located==1.0:
                subset_time.append(length_time_temp)
                evt_mags.append(self.Well[s].mag)
                evt_vert_dist.append(self.Well[s].vert_distance_from_port)
                evt_lat_dist.append(self.Well[s].lat_distance_from_port)
                evt_color.append(hex_int_to_rgb(int(self.Well[s].comp_color)))
                
        max_mag=np.max(evt_mags)
        min_mag=np.min(evt_mags)
        
            
        output=bin_data(well_time,time_interval)
        events=bin_data(subset_time,time_interval)    
        
        fig=plt.figure(figsize=(8+0.8*len(header_test),5))
        host = host_subplot(111, axes_class=AA.Axes)
        plt.subplots_adjust(right=0.75)
        
        
        par=[]
        par.append(host.twinx())    
        offset_inc=60
        offset=0
        
        for s in range(1,len(header_test)):
            
            par.append(host.twinx())    
                    
            offset = offset+offset_inc
            new_fixed_axis = par[s].get_grid_helper().new_fixed_axis
            par[s].axis["right"] = new_fixed_axis(loc="right",
                                                axes=par[s],
                                                offset=(offset, 0))
            
            par[s].axis["right"].toggle(all=True)
            
        host.set_xlim(time_interval[0],length_time)
   
        host.set_xlabel("Time (24 hour) ",fontsize=20)
        host.set_ylabel("Event Count",fontsize=20)
        
        min_dot_size=4
        max_dot_size=18
 
        for s in range(0,len(header_test)):
            par[s].set_ylabel(header_test[s][0])
        
        p=[]
        for s in range(0,len(header_test)):

            elapsed_time=[]
            for k in range(0,len(self.pump_dict['Time'])):
                
                time_temp=self.pump_dict['Time'][k]
                date_temp=self.pump_dict['Date'][k]
                
                num_days=days_between_dates(self.date_start,date_temp)
                elapsed_time.append(float(time_temp)-float(self.time_start)+24*num_days)     
                
            
            p.append(par[s].plot(elapsed_time,self.pump_dict[header_test[s][0]],color=header_test[s][1])[0])
        
        for s in range(0,len(header_test)):
            par[s].set_ylim(0, header_test[s][2])
        
        width=2/float(self.bin_num)
        #0.05
        if self.choice_val1==1:
            host.set_ylim(0, float(self.max_event_count))
            host.set_ylabel("Event Count",fontsize=20)
            host.bar(output[0],output[1],width,label="Triggered",color=[0.588,0.588,0.588])
            host.bar(events[0],events[1],width,label="Located",color=[0.196,0.392,0.588])
        elif self.choice_val1==2:
            if self.choice_val2==1:
                for t in range(0,len(subset_time)):
                    host.set_ylim(0, float(self.max_distance))
                    host.set_ylabel("Vertical Distance (ft)",fontsize=20)
                    size=((max_dot_size-min_dot_size)/(max_mag-min_mag))*(evt_mags[t]-min_mag)+min_dot_size

                    host.plot(subset_time[t],evt_vert_dist[t],'o',color=evt_color[t],ms=size,alpha=0.8)
            elif self.choice_val2==2:
                for t in range(0,len(subset_time)):
                    host.set_ylim(0, float(self.max_distance))
                    host.set_ylabel("Lateral Distance (ft)",fontsize=20)
                    size=((max_dot_size-min_dot_size)/(max_mag-min_mag))*(evt_mags[t]-min_mag)+min_dot_size
                    host.plot(subset_time[t],evt_lat_dist[t],'o',color=evt_color[t],ms=sizw,alpha=0.8)
                    
        host.text(0,-0.10*host.get_ylim()[1],self.date_start,size='12',ha='center')
        
        
        host.legend()
        
        host.axis["left"].label.set_fontsize(16)
        host.axis["bottom"].label.set_fontsize(16)
        
        for s in range(0,len(header_test)):
            par[s].axis["right"].label.set_color(p[s].get_color())
            par[s].axis["right"].label.set_fontsize(16)
            
            
        ticks=host.get_xticks()
        
        new_labels=[]
        elapsed_time_temp=[]
        reduced_time_temp=[]
        flag_new_day=0
        for tick in ticks:
            elapsed_time_temp.append(tick+float(self.time_start))
            
            num_24_hours=np.floor(elapsed_time_temp[-1]/24)
            
            print num_24_hours
            
            reduced_time_temp.append(elapsed_time_temp[-1]-np.floor(elapsed_time_temp[-1]/24)*24)

            time_temp=convert_decimal_time(reduced_time_temp[-1])
            new_labels.append(time_temp)
            
            #time_inc = (elapsed_time_temp[-1]-elapsed_time_temp[-2])
                
            #host.text(tick,-0.10*host.get_ylim()[1],self.date_start,size='12',ha='center')
            
        host.set_xticklabels(new_labels)    
            
        self.plt=plt
        self.par=par
        
        self.ax=pyplot.gca()
        
        plt.tight_layout()
        plt.draw()
        plt.show()
        
        self.blah=host        

root= Tk()

app=App(root)
root.mainloop()


#----------------------------------------------------------------------------




#--------------------------------------------------------------------------
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 14:18:23 2015

@author: Drew
"""


from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt
from Tkinter import *
import tkFileDialog
import os

import MyLibrary.mathfunctions as mf
from MyLibrary.fracture_objects import *


from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import numpy as np
import scipy.special

class App_BuildDFN:
      
    def __init__(self,master):
            
    #Frame.__init__(self, master)
    
    #self.LastClick= Label(self, text= "Last Point Clicked:")
    #self.LastClick.pack(side= LEFT)
    
    #self.CurrentPosition= Label(self, text="Cursor Point:")
    #self.CurrentPosition.pack(side= LEFT)
    
        current_dir=os.popen('cd').read()   #cd is the MS command prompt command for pwd
        current_dir=current_dir[0:-1]  #Remove the /n character
        current_dir.replace("\\", "\\\\") #NOTE: "\\" is actually '\', this has to deal with \ being an escape character
        
        self.current_dir=current_dir   
        self.master=master
          
        master.geometry("400x400")
          
                    
        frame3=Frame(master)  # The Labels
        frame3.pack(side=LEFT)
        
        frame2=Frame(master)  # The entrys
        frame2.pack(side=LEFT)
        
        frame1=Frame(master)   #The hello and quit buttons
        frame1.pack(side=LEFT)
        
        #self.button1 = Button(frame1, text="QUIT", fg="red", command=master.quit)
        #self.button1.pack(side=TOP)
          
        self.button2 = Button(frame1, text="Load Model Inputs", fg="red", command=self.load_model_inputs)
        self.button2.pack(side=TOP)
        
        self.button3 = Button(frame1, text="Load Stress Inputs", fg="red", command=self.load_stress_inputs)
        self.button3.pack(side=TOP)
        
        self.button4 = Button(frame1, text="Rotate DFN", fg="red", command=self.get_rotate_dfn_inputs)
        self.button4.pack(side=TOP)
        
        self.button5 = Button(frame1, text="Plot Failure Curves",fg="red", command=self.plot_failure_curves)
        self.button5.pack(side=TOP)
        
        self.button6 = Button(frame1, text="Generate DFN",fg="red",command=self.generate_dfn)
        self.button6.pack(side=TOP)
        
        self.button7 = Button(frame1, text="Import DFN", fg="red",command=self.import_dfn)
        self.button7.pack(side=TOP)
                    
        self.button8 = Button(frame1, text="Plot DFN", fg="red",command=self.plot_dfn)
        self.button8.pack(side=TOP)
        
        self.button9 = Button(frame1, text="Export DFN", fg="red",command=self.export_dfn)
        self.button9.pack(side=TOP)
        
        self.button10 = Button(frame1, text="Thin DFN",fg="red",command=self.get_thin_dfn_param)   
        self.button10.pack(side=TOP)
        
        self.button11 = Button(frame1, text="Extend DFN",fg="red",command=self.get_extend_dfn_param)   
        self.button11.pack(side=TOP)
        
        self.button12 = Button(frame1,  text="Translate DFN",fg="red",command=self.get_translate_param)
        self.button12.pack(side=TOP)

        self.button13 = Button(frame1, text="Remove DFN",fg="red",command=self.get_remove_dfn_param) 
        self.button13.pack(side=TOP)
        
        self.Model=Model(0,0,0,0,0,0)
        
    def update_dir(self,file_name):

        temp=file_name.split('/')
        main_dir=''
        for s in range(0,len(temp)-1):
            main_dir=main_dir+'/'+temp[s]

        self.current_dir=main_dir
                
    def load_model_inputs(self):
        model_inputs=Toplevel()
        
        model_inputs.model_inputs_frame1=Frame(model_inputs)
        model_inputs.model_inputs_frame1.pack(side=TOP)
        
        model_inputs.model_inputs_frame1.label1=Label(model_inputs.model_inputs_frame1, text='Model x length')
        model_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame1.e1=Entry(model_inputs.model_inputs_frame1)
        model_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame1.label2=Label(model_inputs.model_inputs_frame1, text='Model y length')
        model_inputs.model_inputs_frame1.label2.pack(side=LEFT)
        
        model_inputs.model_inputs_frame1.e2=Entry(model_inputs.model_inputs_frame1)
        model_inputs.model_inputs_frame1.e2.pack(side=LEFT)

        model_inputs.model_inputs_frame1.label3=Label(model_inputs.model_inputs_frame1, text='Model z length')
        model_inputs.model_inputs_frame1.label3.pack(side=LEFT)
        
        model_inputs.model_inputs_frame1.e3=Entry(model_inputs.model_inputs_frame1)
        model_inputs.model_inputs_frame1.e3.pack(side=LEFT)

        model_inputs.model_inputs_frame2=Frame(model_inputs)
        model_inputs.model_inputs_frame2.pack(side=TOP)  
        
        model_inputs.model_inputs_frame3=Frame(model_inputs)
        model_inputs.model_inputs_frame3.pack(side=TOP)  
        
        model_inputs.model_inputs_frame3.label1=Label(model_inputs.model_inputs_frame3, text='Model center x')
        model_inputs.model_inputs_frame3.label1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame3.e1=Entry(model_inputs.model_inputs_frame3)
        model_inputs.model_inputs_frame3.e1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame3.label1=Label(model_inputs.model_inputs_frame3, text='Model center y')
        model_inputs.model_inputs_frame3.label1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame3.e2=Entry(model_inputs.model_inputs_frame3)
        model_inputs.model_inputs_frame3.e2.pack(side=LEFT)
        
        model_inputs.model_inputs_frame3.label1=Label(model_inputs.model_inputs_frame3, text='Model center z')
        model_inputs.model_inputs_frame3.label1.pack(side=LEFT)
        
        model_inputs.model_inputs_frame3.e3=Entry(model_inputs.model_inputs_frame3)
        model_inputs.model_inputs_frame3.e3.pack(side=LEFT)
        
        
        model_inputs.e_button1=Button(model_inputs, text="Confirmation", fg="red", command= lambda: self.store_model_values(model_inputs))
        model_inputs.e_button1.pack(side=TOP)
        
        model_inputs.button2=Button(model_inputs, text="Cancel", fg="red", command=model_inputs.destroy)
        model_inputs.button2.pack(side=TOP)        
        
    def load_stress_inputs(self):
        
        stress_inputs = Toplevel()
        
        stress_inputs.model_inputs_frame1=Frame(stress_inputs)
        stress_inputs.model_inputs_frame1.pack(side=TOP)
        
        stress_inputs.model_inputs_frame1.label1=Label(stress_inputs.model_inputs_frame1, text='Sxx')
        stress_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame1.e1=Entry(stress_inputs.model_inputs_frame1)
        stress_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame1.label2=Label(stress_inputs.model_inputs_frame1, text='Syy')
        stress_inputs.model_inputs_frame1.label2.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame1.e2=Entry(stress_inputs.model_inputs_frame1)
        stress_inputs.model_inputs_frame1.e2.pack(side=LEFT)

        stress_inputs.model_inputs_frame1.label3=Label(stress_inputs.model_inputs_frame1, text='Szz')
        stress_inputs.model_inputs_frame1.label3.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame1.e3=Entry(stress_inputs.model_inputs_frame1)
        stress_inputs.model_inputs_frame1.e3.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2=Frame(stress_inputs)
        stress_inputs.model_inputs_frame2.pack(side=TOP)

        stress_inputs.model_inputs_frame2.label4=Label(stress_inputs.model_inputs_frame2, text='Sxy')
        stress_inputs.model_inputs_frame2.label4.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2.e4=Entry(stress_inputs.model_inputs_frame2)
        stress_inputs.model_inputs_frame2.e4.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2.label5=Label(stress_inputs.model_inputs_frame2, text='Sxz')
        stress_inputs.model_inputs_frame2.label5.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2.e5=Entry(stress_inputs.model_inputs_frame2)
        stress_inputs.model_inputs_frame2.e5.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2.label6=Label(stress_inputs.model_inputs_frame2, text='Syz')
        stress_inputs.model_inputs_frame2.label6.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame2.e6=Entry(stress_inputs.model_inputs_frame2)
        stress_inputs.model_inputs_frame2.e6.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame3=Frame(stress_inputs)
        stress_inputs.model_inputs_frame3.pack(side=TOP)
        
        stress_inputs.model_inputs_frame3.label6=Label(stress_inputs.model_inputs_frame3, text='Pore Pressure')
        stress_inputs.model_inputs_frame3.label6.pack(side=LEFT)
        
        stress_inputs.model_inputs_frame3.e7=Entry(stress_inputs.model_inputs_frame3)
        stress_inputs.model_inputs_frame3.e7.pack(side=LEFT)
        
        stress_inputs.e_button1=Button(stress_inputs, text="Confirmation", fg="red", command= lambda: self.store_stress_values(stress_inputs))
        stress_inputs.e_button1.pack(side=TOP)
        
        stress_inputs.button2=Button(stress_inputs, text="Cancel", fg="red", command=stress_inputs.destroy)
        stress_inputs.button2.pack(side=TOP)   
     
        
    def store_model_values(self,model_inputs):
        
        self.Model=Model(float(model_inputs.model_inputs_frame1.e1.get()),float(model_inputs.model_inputs_frame1.e2.get()),float(model_inputs.model_inputs_frame1.e3.get()),float(model_inputs.model_inputs_frame3.e1.get()),float(model_inputs.model_inputs_frame3.e2.get()),float(model_inputs.model_inputs_frame3.e3.get()))

        model_inputs.destroy()    
        
    def store_stress_values(self,stress_inputs):
        
        self.Model.set_model_stress(float(stress_inputs.model_inputs_frame1.e1.get()),float(stress_inputs.model_inputs_frame1.e2.get()),float(stress_inputs.model_inputs_frame1.e3.get()),float(stress_inputs.model_inputs_frame2.e4.get()),float(stress_inputs.model_inputs_frame2.e5.get()),float(stress_inputs.model_inputs_frame2.e6.get()))
        self.Model.set_pore_pressure(float(stress_inputs.model_inputs_frame3.e7.get()))
        
        stress_inputs.destroy()
               
    def load_prob_func(self):
        
        prob_func_wgt=Toplevel()
        
        prob_func_wgt.entry_labels=['A','B','Start x','Start y','Increment']

        prob_func_wgt.e=[]
        prob_func_wgt.lbl=[]        
        
        prob_func_wgt.label1=Label(prob_func_wgt,text='Gamma Probability Density Function')
        prob_func_wgt.label1.pack(side=TOP)
        
        
        for s in range(0,len(prob_func_wgt.entry_labels)):
            
            prob_func_wgt.lbl.append(Label(prob_func_wgt,text=prob_func_wgt.entry_labels[s]))
            prob_func_wgt.lbl[-1].pack(side=LEFT)
            
            prob_func_wgt.e.append(Entry(prob_func_wgt))
            prob_func_wgt.e[-1].pack(side=LEFT)
            
        prob_func_wgt.e_button1=Button(prob_func_wgt, text="Confirmation", fg="red", command= lambda: self.store_prob_func(prob_func_wgt))
        prob_func_wgt.e_button1.pack(side=TOP)
        
        prob_func_wgt.button2=Button(prob_func_wgt, text="Cancel", fg="red", command=prob_func_wgt.destroy)
        prob_func_wgt.button2.pack(side=TOP)
        
    def store_prob_func(self,wgt):
        
        
        self.M.set_probability_function(float(wgt.e[0].get()),float(wgt.e[1].get()),float(wgt.e[2].get()),float(wgt.e[3].get()),float(wgt.e[4].get()))
        
        wgt.destroy()
        
    def get_dfn_inputs(self):
                
        dfn_input_wgt=Toplevel()       
        dfn_input_wgt.entry_labels=['Fracture Orientations','Dip','Number of fractures','Area Min','Area Max','Thin_Flag','Min Centroid Distance','Min parallel fracture distance']
        
        dfn_input_wgt.e=[]
        dfn_input_wgt.lbl=[]
        
        dfn_input_wgt.label1=Label(dfn_input_wgt,text='DFN Generation Inputs')
        dfn_input_wgt.label1.pack(side=TOP)
        
        for s in range(0,len(dfn_input_wgt.entry_labels)):
            
            dfn_input_wgt.lbl.append(Label(dfn_input_wgt,text=dfn_input_wgt.entry_labels[s]))
            dfn_input_wgt.lbl[-1].pack(side=TOP)
            
            dfn_input_wgt.e.append(Entry(dfn_input_wgt))
            dfn_input_wgt.e[-1].pack(side=TOP)
            
        dfn_input_wgt.e_button1=Button(dfn_input_wgt, text="Confirmation", fg="red", command= lambda: self.store_dfn_inputs(dfn_input_wgt))
        dfn_input_wgt.e_button1.pack(side=TOP)
        
        dfn_input_wgt.button2=Button(dfn_input_wgt, text="Cancel", fg="red", command=dfn_input_wgt.destroy)
        dfn_input_wgt.button2.pack(side=TOP)
        
    def store_dfn_inputs(self,wgt):
        
        strike_azimuths = list_float(wgt.e[0].get().split(','))
        dip = list_float(wgt.e[1].get().split(','))
        frac_num = int(wgt.e[2].get())
        area_min = float(wgt.e[3].get())
        area_max = float(wgt.e[4].get())
        thin_dfn_flag = int(wgt.e[5].get())
        min_cent_dist = float(wgt.e[6].get())
        min_parallel_dist = float(wgt.e[7].get())
        
        self.Model.generate_dfn()
        self.Model.loaded_DFN.set_build_parameters(strike_azimuths,dip,frac_num,area_min,area_max,thin_dfn_flag,min_cent_dist,min_parallel_dist)
        self.Model.loaded_DFN.generate_fractures()
        
        wgt.destroy()
        
    def generate_dfn(self):
        self.get_dfn_inputs()
                             
    def plot_dfn(self):   
        
        self.Model.loaded_DFN.plot_DFN()
        
    def import_dfn(self):
        
        import_dfn_file = tkFileDialog.askopenfilename(filetypes = (("Template files", "*.dat")
                                                             ,("All files", "*.*") ),initialdir=self.current_dir)
                                                             
        self.update_dir(import_dfn_file)                           
        self.Model.generate_dfn()
        self.Model.loaded_DFN.import_Fractures(import_dfn_file)
        
    def export_dfn(self):
        
        export_dfn_file = tkFileDialog.asksaveasfilename(filetypes = (("Template files", "*.dat")
                                                             ,("All files", "*.*") ),initialdir=self.current_dir)
                                                             
        self.update_dir(export_dfn_file)
        self.Model.loaded_DFN.export_Fractures(export_dfn_file)
        
    def get_rotate_dfn_inputs(self):
        
        rot_dfn=Toplevel()
        rot_dfn.geometry("400x100")
        
        choice_frame1=Frame(rot_dfn)
        choice_frame1.pack(side=TOP)         
        
        txt = Label(choice_frame1,text='Rotation angle (** ***)')
        txt.pack(side=LEFT)
        
        rot_dfn.ent1 = Entry(choice_frame1)
        rot_dfn.ent1.pack(side=LEFT)        
        
        self.choice_val1=1
       
        self.v1=IntVar()
        self.v1.set(1)
        
        choice_frame2=Frame(rot_dfn)
        choice_frame2.pack(side=TOP)  
        
        b1=Radiobutton(choice_frame2,text='Rotate DFN',padx=20,variable=self.v1,command=self.choose_choice,value=1)
        b1.pack(side=LEFT)
        txt = Label(choice_frame2,text='About rotation pt (x,y,z)')
        txt.pack(side=LEFT)
        rot_dfn.ent2 = Entry(choice_frame2)
        rot_dfn.ent2.pack(side=LEFT)
        
        choice_frame3=Frame(rot_dfn)
        choice_frame3.pack(side=TOP)  
               
        b2=Radiobutton(choice_frame3,text='Rotate Fractures',padx=20,variable=self.v1,command=self.choose_choice,value=2)
        b2.pack(side=LEFT)
        
        rot_dfn.button1=Button(rot_dfn,text='Done',command = lambda: self.rotate_dfn(rot_dfn))
        rot_dfn.button1.pack(side=BOTTOM)
        
    def choose_choice(self):       
        self.choice_val1=self.v1.get()
        
    def rotate_dfn(self,rot_dfn):
        
        rotation_value = float(rot_dfn.ent1.get())
        
        if self.choice_val1==1:
            rotation_pt = mf.list_float(rot_dfn.ent2.get().split(','))
            self.Model.loaded_DFN.rotate_DFN(rotation_value,rotation_pt)
        else:
            self.Model.loaded_DFN.rotate_Fractures(rotation_value)
            
        rot_dfn.destroy()
    
    def plot_failure_curves(self):
        
        self.Model.plot_failure_curves()
        
        
    def get_thin_dfn_param(self):
        
        thin_inputs = Toplevel()
        
        thin_inputs.model_inputs_frame1=Frame(thin_inputs)
        thin_inputs.model_inputs_frame1.pack(side=TOP)
        
        thin_inputs.model_inputs_frame1.label1=Label(thin_inputs.model_inputs_frame1, text='Remove Percentage: ')
        thin_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        thin_inputs.model_inputs_frame1.e1=Entry(thin_inputs.model_inputs_frame1)
        thin_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        thin_inputs.e_button1=Button(thin_inputs, text="Confirmation", fg="red", command= lambda: self.thin_dfn(thin_inputs))
        thin_inputs.e_button1.pack(side=TOP)
        
        thin_inputs.button2=Button(thin_inputs, text="Cancel", fg="red", command=thin_inputs.destroy)
        thin_inputs.button2.pack(side=TOP)  
        
    def thin_dfn(self,thin_inputs):
        
        percentage = float(thin_inputs.model_inputs_frame1.e1.get())
        
        self.Model.loaded_DFN.thin_dfn_uniform(percentage)        
        
        thin_inputs.destroy()
        
    def get_extend_dfn_param(self):
        
        thin_inputs = Toplevel()
        
        thin_inputs.model_inputs_frame1=Frame(thin_inputs)
        thin_inputs.model_inputs_frame1.pack(side=TOP)
        
        thin_inputs.model_inputs_frame1.label1=Label(thin_inputs.model_inputs_frame1, text='Extend Percentage: ')
        thin_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        thin_inputs.model_inputs_frame1.e1=Entry(thin_inputs.model_inputs_frame1)
        thin_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        self.choice_val1=1
       
        self.v1=IntVar()
        self.v1.set(1)
        
        b1=Radiobutton(thin_inputs,text='x direction',padx=20,variable=self.v1,command=self.choose_choice,value=1)
        b1.pack(side=LEFT)
        
        b3=Radiobutton(thin_inputs,text='y direction',padx=20,variable=self.v1,command=self.choose_choice,value=3)
        b3.pack(side=LEFT)
                
        b2=Radiobutton(thin_inputs,text='z direction',padx=20,variable=self.v1,command=self.choose_choice,value=2)
        b2.pack(side=LEFT)        
        
        thin_inputs.e_button1=Button(thin_inputs, text="Confirmation", fg="red", command= lambda: self.extend_dfn(thin_inputs))
        thin_inputs.e_button1.pack(side=TOP)
        
        thin_inputs.button2=Button(thin_inputs, text="Cancel", fg="red", command=thin_inputs.destroy)
        thin_inputs.button2.pack(side=TOP)  
        
    def extend_dfn(self,thin_inputs):
        
        if self.choice_val1==1:
            dimen='x'
        if self.choice_val1==3:
            dimen='y'
        if self.choice_val1==2:
            dimen='z'
            
        
        percentage = float(thin_inputs.model_inputs_frame1.e1.get())
        
        value = percentage/100.0
        
        self.Model.loaded_DFN.extend_dfn(value,dimension=dimen)        
        
        thin_inputs.destroy()
        
    def get_translate_param(self):
        
        translate_inputs = Toplevel()
        
        translate_inputs.model_inputs_frame1=Frame(translate_inputs)
        translate_inputs.model_inputs_frame1.pack(side=TOP)
        
        translate_inputs.model_inputs_frame1.label1=Label(translate_inputs.model_inputs_frame1, text='Translate (x,y,z): ')
        translate_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        translate_inputs.model_inputs_frame1.e1=Entry(translate_inputs.model_inputs_frame1)
        translate_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        translate_inputs.e_button1=Button(translate_inputs, text="Confirmation", fg="red", command= lambda: self.translate_dfn(translate_inputs))
        translate_inputs.e_button1.pack(side=TOP)
        
        translate_inputs.button2=Button(translate_inputs, text="Cancel", fg="red", command=translate_inputs.destroy)
        translate_inputs.button2.pack(side=TOP)  
        
    def translate_dfn(self,translate_inputs):
            
        value = translate_inputs.model_inputs_frame1.e1.get()
    
        self.Model.loaded_DFN.translate_Fractures(value)        
        translate_inputs.destroy()
        
    def get_remove_dfn_param(self):
        
        remove_dfn_inputs = Toplevel()
        
        remove_dfn_inputs.model_inputs_frame1=Frame(remove_dfn_inputs)
        remove_dfn_inputs.model_inputs_frame1.pack(side=TOP)
        
        remove_dfn_inputs.model_inputs_frame1.label1=Label(remove_dfn_inputs.model_inputs_frame1, text='Remove Interval: ')
        remove_dfn_inputs.model_inputs_frame1.label1.pack(side=LEFT)
        
        remove_dfn_inputs.model_inputs_frame1.e1=Entry(remove_dfn_inputs.model_inputs_frame1)
        remove_dfn_inputs.model_inputs_frame1.e1.pack(side=LEFT)
        
        self.choice_val1=1
       
        self.v1=IntVar()
        self.v1.set(1)
        
        b1=Radiobutton(remove_dfn_inputs,text='x direction',padx=20,variable=self.v1,command=self.choose_choice,value=1)
        b1.pack(side=LEFT)
        
        b3=Radiobutton(remove_dfn_inputs,text='y direction',padx=20,variable=self.v1,command=self.choose_choice,value=3)
        b3.pack(side=LEFT)
                
        b2=Radiobutton(remove_dfn_inputs,text='z direction',padx=20,variable=self.v1,command=self.choose_choice,value=2)
        b2.pack(side=LEFT)        
        
        remove_dfn_inputs.e_button1=Button(remove_dfn_inputs, text="Confirmation", fg="red", command= lambda: self.remove_dfn(remove_dfn_inputs))
        remove_dfn_inputs.e_button1.pack(side=TOP)
        
        remove_dfn_inputs.button2=Button(remove_dfn_inputs, text="Cancel", fg="red", command=remove_dfn_inputs.destroy)
        remove_dfn_inputs.button2.pack(side=TOP)  
        
        
    def remove_dfn(self,remove_dfn_inputs):
        
        if self.choice_val1==1:
            dimen='x'
        if self.choice_val1==3:
            dimen='y'
        if self.choice_val1==2:
            dimen='z'
            
        print dimen
        
        interval = remove_dfn_inputs.model_inputs_frame1.e1.get().split(',') 
        interval = [float(interval[0]),float(interval[1])]
        
        self.Model.loaded_DFN.remove_fracture_interval(interval,dimension=dimen)        
        
        remove_dfn_inputs.destroy()
    
        
root= Tk()

app=App_BuildDFN(root)
root.mainloop()
    
